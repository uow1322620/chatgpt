package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button jump_to_chat_bt;
    Button jump_to_image_bt;
    ImageButton communityBt;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    jump_to_chat_bt = findViewById(R.id.jump_to_chat);
    jump_to_image_bt = findViewById(R.id.jump_to_image);
    jump_to_chat_bt.setOnClickListener((v)->{
        Intent intent = new Intent(MainActivity.this,chatActivity.class);
        startActivity(intent);
    });
        jump_to_image_bt.setOnClickListener((v)->{
        Intent intent = new Intent(MainActivity.this,imageActivity.class);
        startActivity(intent);
    });
        communityBt = findViewById(R.id.community);
        communityBt.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this,communityActivity.class);
            startActivity(intent);
        });

    }
}

