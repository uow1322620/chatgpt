package com.example.myapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class imageActivity extends AppCompatActivity {
    private static Context context;
    public static final String API_KEY = "zhSD0QvycSaj2MBsfLfcDBPs";
    public static final String SECRET_KEY = "it2U7gfjPlfsE1hbBQa0NVHlnZlQ5I2l";
    //定义需要的权限
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_MEDIA_IMAGES,//读取外部存储
            Manifest.permission.WRITE_EXTERNAL_STORAGE};//写入外部存储
    private static int REQUEST_PERMISSION_CODE = 2;

    static final OkHttpClient HTTP_CLIENT = new OkHttpClient().newBuilder().build();

    ImageView ivChoose,ivTake;
    ImageButton btn_c,btn_t;
    static TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_image);
        getPermission(this);
        ivChoose = findViewById(R.id.selected_image);
//        ivTake = findViewById(R.id.imageViewTake);
        btn_c = findViewById(R.id.image_album_bt);
//        btn_t = findViewById(R.id.buttonTakePhoto);
        textView = findViewById(R.id.resultText);

        btn_c.setOnClickListener(this::btnClickChoose);
//        btn_t.setOnClickListener(this::btnClickTake);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }


    ActivityResultLauncher<Intent> ChooseImg = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == RESULT_OK)
                    {
                        if(result.getData() != null)
                        {

                            Uri imuri = result.getData().getData();
                            ivChoose.setImageURI(imuri);

                            String path = FileUtils.getFilePathByUri(context,imuri);
                            Log.d("result","accurateBasic(path)");
                            accurateBasic(path);

                        }
                    }
                }
            }
    );


    public void btnClickChoose(View v) {//选择照片
        Intent Album = new Intent(Intent.ACTION_GET_CONTENT);
        //ACTION_GET_CONTENT是获取本地所有图片，ACTION_PICK获取的是相册中的图片
        Album.setType("image/*");
        //设置图像类型
        ChooseImg.launch(Album);

    }
public static void accurateBasic(String filePath) {
    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
    // image 可以通过 getFileContentAsBase64("C:\fakepath\b378281cf8f43511d4d3894dba53a64.png") 方法获取,如果Content-Type是application/x-www-form-urlencoded时,第二个参数传true
    Request request = null;
    try {
        RequestBody body = RequestBody.create(mediaType, "image="+getFileContentAsBase64(filePath,true));
        request = new Request.Builder()
                .url("https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token=" + getAccessToken())
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .build();
    } catch (IOException e) {
        throw new RuntimeException(e);
    }
    Response response = null;
    JSONObject jsonObject = null;
    try {
        response = HTTP_CLIENT.newCall(request).execute();
        jsonObject = new JSONObject(response.body().string());
        JSONArray jsonArray = jsonObject.getJSONArray("words_result");
        String result = "";
        for(int i = 0;i<jsonArray.length();i++){
            result += jsonArray.getJSONObject(i).getString("words");
        }
        textView.setText(result);
        System.out.println(result);
    } catch (IOException | JSONException e) {
        throw new RuntimeException(e);

    }

}

    static String getAccessToken() throws IOException {
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials&client_id=" + API_KEY
                + "&client_secret=" + SECRET_KEY);
        Request request = new Request.Builder()
                .url("https://aip.baidubce.com/oauth/2.0/token")
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        Response response = HTTP_CLIENT.newCall(request).execute();
        try {
            return new JSONObject(response.body().string()).getString("access_token");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
    static String getFileContentAsBase64(String path, boolean urlEncode) throws IOException {

        String base64 = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            byte[] b = Files.readAllBytes(Paths.get(path));
            base64 = Base64.getEncoder().encodeToString(b);
        }
        if (urlEncode) {
            base64 = URLEncoder.encode(base64, "utf-8");
        }
        return base64;
    }

    public void getPermission(Activity obj) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (int i = 0; i < PERMISSIONS_STORAGE.length; i++) {
                if (ContextCompat.checkSelfPermission(obj, PERMISSIONS_STORAGE[i])
                        != PackageManager.PERMISSION_GRANTED) {
                    // 请求权限
                    ActivityCompat.requestPermissions(obj, PERMISSIONS_STORAGE, REQUEST_PERMISSION_CODE);
                    break;
                }
            }
        }
    }


}