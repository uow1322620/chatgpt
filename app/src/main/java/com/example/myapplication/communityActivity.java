package com.example.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class communityActivity extends AppCompatActivity {
    ImageButton community_edit;
    List<News> mNewsList = new ArrayList<>();
    RecyclerView mRecyclerView;
    MyAdapter mMyAdapter ;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);
        community_edit = findViewById(R.id.community_edit_btn);
        mRecyclerView = findViewById(R.id.recycler_community);
        community_edit.setOnClickListener(view -> {
            Intent intent = new Intent(communityActivity.this,edit_communityActivity.class);
            intent.putExtra("newslist",(Serializable)mNewsList);
            startActivity(intent);

        });

        SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String data = prefs.getString("data", "");
        Intent intent = getIntent();
        String text = intent.getStringExtra("text");
        News news = new News();
        news.content = text;
        mNewsList.add(news);
        mMyAdapter = new MyAdapter();
        mMyAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mMyAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(communityActivity.this);
        mRecyclerView.setLayoutManager(layoutManager);



    }
    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("data", "Hello World");
        editor.apply();
    }
    class MyAdapter extends RecyclerView.Adapter<MyViewHoder> {

        @NonNull
        @Override
        public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = View.inflate(communityActivity.this, R.layout.item_list, null);
            MyViewHoder myViewHoder = new MyViewHoder(view);
            return myViewHoder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
            News news = mNewsList.get(position);
//            holder.mTitleim.setImageURI();
            holder.mTitleContent.setText(news.content);
        }

        @Override
        public int getItemCount() {
            return mNewsList.size();
        }
    }

    class MyViewHoder extends RecyclerView.ViewHolder {
        ImageView mTitleim;
        TextView mTitleContent;

        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            mTitleim = itemView.findViewById(R.id.imageview);
            mTitleContent = itemView.findViewById(R.id.textView2);
        }
    }

}
