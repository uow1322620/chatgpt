package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class edit_communityActivity extends AppCompatActivity {
    Button community_send;
    TextView edit_text_community;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_community);
        community_send = findViewById(R.id.community_send_btn);
        edit_text_community = findViewById(R.id.edit_text_community_btn);

        community_send.setOnClickListener(view -> {
            String text = edit_text_community.getText().toString();
            Intent intent = new Intent(edit_communityActivity.this,communityActivity.class);
            intent.putExtra("text",text);

            startActivity(intent);

        });
    }


}
